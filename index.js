//console.log("You are now linked");


// FIRST OUTPUT

let userNumber = Number(prompt("Please enter a number: "));
console.log("The number you provided is " +userNumber+ ".");

for(let index = userNumber; index >= 0; index--) {

	if(index%10 === 0 && index != 50){
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}

	if(index%5 === 0 && index != 50){
		console.log(index);
		continue;
	}

	if(index <= 50 ) {
		console.log("The current value is at 50. Terminating the loop.");
		break;
	}

}

// SECOND OUTPUT

let string = "supercalifragilisticexpialidocious";
console.log(string);
let newString = "";
let b = 0;

for(let a = 0; a < string.length; a++) {
	if(string[a].toLowerCase() === "a" ||
		string[a].toLowerCase() === "e" ||
		string[a].toLowerCase() === "i" ||
		string[a].toLowerCase() === "o" ||
		string[a].toLowerCase() === "u") {
		newString += "";
	} else {
		 newString += string[a];
	}
}

console.log(newString);
 


